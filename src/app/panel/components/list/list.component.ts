import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

//modelo
import { Topic } from '../../../models/Topic';
//servicios
import { UserService } from '../../../services/user.service';
import { TopicService } from '../../../services/topic.service';

@Component({
  selector: 'panel-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [UserService, TopicService],
})
export class ListComponent implements OnInit {
  public pageTitle: string;
  public topics:Array<Topic>;
  public identity;
  public token;
  public status: string;
  public message: string;

  constructor(private _userService: UserService,
    private _topicService: TopicService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) {

      this.pageTitle="Mis temas"
      this.token=this._userService.getToken();
      this.identity=this._userService.getIdentity();


  }

  ngOnInit() {
    this.getMyTopics();
  }

  getMyTopics(){
    this._topicService.getMyTopics(this.token,this.identity._id).subscribe(
      response=>{
        if(response.topics){
          console.log(response.topics);
          this.topics=response.topics;
        }else{
          this.status='error';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message=error.error.message;
      }
    );
  }

  delete(topicId){
    this._topicService.deleteTopic(topicId,this.token).subscribe(
      response=>{
        if(response.topic){
          this.status='success';
          this.getMyTopics();
          setTimeout(()=>{
            this.status=null;
          },3000);
        }else{
          this.status='error';
        }
        this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );
    
  }
}
