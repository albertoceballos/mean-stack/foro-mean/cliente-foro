import { Injectable } from '@angular/core';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

//modelos
import {User} from '../models/User';

//url
import {GLOBAL} from '../services/url';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url:string;
  public identity;
  public token;
  constructor(private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
  }

 

  //Conseguir identidad de usuario logueado
  getIdentity(){
    let identity=JSON.parse(localStorage.getItem('identity'));
    
    if(identity && identity!=null && identity!=undefined){
      this.identity=identity;
    }else{
      this.identity=null;
    }

    return this.identity;
  }

  //conseguir token de usuario identificado
  getToken(){
    let token=localStorage.getItem('token');

    if(token && token!=null && token!=undefined){
      this.token=token;
    }else{
      this.token=null;
    }

    return this.token;
    
  }

  register(user):Observable<any>{
    //convertir el objeto de usuario a un JSON string
    let params=JSON.stringify(user);
    //Definir cabeceras
    let headers=new HttpHeaders().set('Content-Type','application/json');
    //pertición Ajax
    return this._httpClient.post(this.url+'save',params,{headers:headers});
  }

  //método para login
  signUp(user, getToken=null):Observable<any>{
    if(getToken!=null){
      user.getToken=getToken;
    }
    let params=JSON.stringify(user);
    let headers=new HttpHeaders().set('Content-Type','application/json');

    return this._httpClient.post(this.url+'login',params,{headers:headers});
  }

  upload(user):Observable<any>{

    let params=JSON.stringify(user);
    let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',this.getToken());

    return this._httpClient.put(this.url+'user/update',params,{headers:headers});
  }

  //Todos los usuarios
  getUsers():Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',this.getToken());

    return this._httpClient.get(this.url+'users',{headers:headers});
  }

  //usuario por ID
  getUser(userId):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/json').set('Authorization',this.getToken());

    return this._httpClient.get(this.url+'user/'+userId,{headers:headers});

  }


}
