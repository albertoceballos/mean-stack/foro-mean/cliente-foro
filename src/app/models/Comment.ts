export class Comment{
    public _id:string;
    public content:string;
    public date:string;
    public user:any;

    constructor(id,content,date,user){
        this._id=id;
        this.content=content;
        this.date=date;
        this.user=user;
    }
    
}