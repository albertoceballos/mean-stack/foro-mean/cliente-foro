
export class User{
    public _id:string;
    public name:string;
    public surname:string;
    public email:string;
    public password:string;
    public image:string;
    public role:string;

    constructor(id,name,surname,email,password,image,role){
        this._id=id;
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.password=password;
        this.image=image;
        this.role=role;
    }

}