
export class Topic{
    public _id:string;
    public title:string;
    public content:string;
    public code:string;
    public lang:string;
    public date:string;
    public user:any;
    public comments:any;

    constructor(id,title,content,code,lang,date,user,comments){
        this._id=id;
        this.title=title;
        this.content=content;
        this.code=code;
        this.lang=lang;
        this.date=date;
        this.user=user;
        this.comments=comments;

    }
}