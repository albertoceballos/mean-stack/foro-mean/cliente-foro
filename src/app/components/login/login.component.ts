import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
//modelo
import {User} from '../../models/User';
//servicios
import {UserService} from '../../services/user.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[UserService],
})
export class LoginComponent implements OnInit {

  public title:string;
  public user:User;
  public status:string;
  public message:string;
  public identity;
  public token;

  constructor(private _userService:UserService,private _router:Router) {
    this.title='Login';
    this.user=new User('','','','','','','ROLE_USER');
   }

  ngOnInit() {
  }

  onSubmit(){
    this._userService.signUp(this.user).subscribe(
      response=>{
        if(response.user && response.user._id){
          this.status='success';
          this.message='Usuario identificado correctamente';
          setTimeout(()=>{
            this._router.navigate(['/inicio']);
          },2500);
          //guardamos la identidad:
          this.identity=response.user;
         //console.log(this.identity);
          //guardamos la identidad en el localStorage
          localStorage.setItem('identity',JSON.stringify(this.identity));

          //conseguir token del usuario
          this._userService.signUp(this.user,true).subscribe(
            response=>{
              if(response.token){
                this.token=response.token;
                //console.log(this.token);
                 //guardamos el token en el localStorage
                localStorage.setItem('token',this.token);
              }
            },
            error=>{
              console.log(error);
            }
          );
        }else{
          this.status='error';
          this.message=response.message;
        }
      },
      error=>{
        console.log(error);
        this.message=error.error.message;
        this.status='error';
      }
    );
  }

}
