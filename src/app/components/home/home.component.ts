import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public title;
  constructor() {
    this.title = 'Bienvenido al foro de programación NgForo';
  }

  ngOnInit() {
  }

}
