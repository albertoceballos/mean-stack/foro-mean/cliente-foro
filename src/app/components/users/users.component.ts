import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {GLOBAL} from '../../services/url';
//modelo
import {User} from '../../models/User';
//servicio
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers:[UserService],
})
export class UsersComponent implements OnInit {
  public title:string;
  public users:Array<User>;
  public url;
  public status:string;
  

  constructor(private _userService:UserService,private _router:Router) {
    this.title="Usuarios";
    this.url=GLOBAL.url;
   }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(){
    this._userService.getUsers().subscribe(
      response=>{
        console.log(response);
        this.users=response.users;
      },
      error=>{
        console.log(error);
      }
    );
  }

}
