import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//modelos
import { Topic } from '../../models/Topic';

//servicios
import { TopicService } from '../../services/topic.service';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css'],
  providers: [TopicService, UserService],
})
export class TopicsComponent implements OnInit {
  public pageTitle: string;
  public status: string;
  public identity;
  public token;
  public topics: Array<Topic>;
  public page;
  public prevPage;
  public nextPage;
  public pages;
  public arrayPages:Array<number>;
  public total;
  public message;


  constructor(private _topicService: TopicService,
     private _userService: UserService,
     private _router:Router,
     private _activatedRoute:ActivatedRoute) {

      this.pageTitle="Temas del foro";
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        var page=params['page'];
        this.getTopics(page);
      }
    );
    
  }

  //todos los topics
  getTopics(page){
    this._topicService.getTopics(page).subscribe(
      response=>{
        if(response.topics){
          this.status='success';
          this.topics=response.topics;
          this.page=response.page;
          this.total=response.total;
          this.pages=response.pages;
          this.arrayPages=[];
          for(let i=1;i<=this.pages;i++){
            this.arrayPages.push(i);
          }
          this.prevPage=response.prevPage;
          this.nextPage=response.nextPage;
          console.log(this.arrayPages);
        }else{
          this.status='error';
        }
        this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );

  }

  
}
